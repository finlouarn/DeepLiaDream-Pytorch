# DeepLiaDream
Adaptation of Google Deep Dream to an artistic project.
A camera in taking live pictures of micro algae that are themselves making a negative picture from the light they receive. 
In the meantime the machine is making live dreams from the pictures it receives at a regular time step from the camera.

It is implemented in Pytorch, making use of ResNet 18, 34 or 50, pretrained on ImageNet.
